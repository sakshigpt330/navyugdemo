import React from 'react';

function ShowError(props) {
    return <div>{props.error || "error"}</div>
}

export default ShowError;
import React from 'react';
import './users.css';
import Loader from '../../components/loader';
import ShowError from '../../components/showError';

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchId: 0,
    }
    props.fetchUsers();
  }

  render() {
    const { usersReducer, history, postReducer } = this.props;
    const { users, loading, error } = usersReducer;
    if(loading) {
      return <Loader />
    }
    if(error) {
      return <ShowError error={error} />
    }
    return (
      <div>
        {users.map(user => {
          const { id, email, name, phone, website } = user;
          const { fetchPost } = this.props;
          const { loading, error } = postReducer;
          const { fetchId } = this.state;
          return (
            <div
              key={id}
              className="user"
            >
              <h5>{name}</h5>
              <span className="link">{email}</span>
              <span className="link">{phone}</span>
              <span className="link">{website}</span>
              <button
                onClick={
                  () => {
                    fetchPost(id, history)
                    this.setState({ fetchId: id });
                  }
                }
              >
                <i>{loading && (fetchId === id) ? "Loading" : "Details"}</i>
              </button>
              {error && (fetchId === id) && <ShowError error={error} />}
            </div>
          )
        })}
      </div>
    );
  }
}

export default Users;

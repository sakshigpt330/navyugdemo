import { connect } from 'react-redux';
import { fetchUsers } from '../../model/users/actions';
import { fetchPost } from '../../model/post/actions';
import Users from './users';

const mapStateToProps = (store) => {
    return({
      usersReducer: store.usersReducer,
      postReducer: store.postReducer
    })
};

const mapDispatchToProps = dispatch => {
  return {
    fetchUsers: () => {dispatch(fetchUsers());},
    fetchPost: (userId, history) => {dispatch(fetchPost(userId, history));}
  };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Users);
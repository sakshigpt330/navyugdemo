import { connect } from 'react-redux';
import UserPost from './userPost';

const mapStateToProps = (store) => {
    return({
        postReducer: store.postReducer
    })
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserPost);
import React from 'react';
import './userPost.css';

class UserPost extends React.Component {

  render() {
    const { postReducer, history } = this.props;
    const { post } = postReducer;
    return (
      <div className="wrapper">
        <button
          onClick={
            () => {
              history.goBack();
            }
          }
        >
          <i>Back</i>
        </button>
        <div>
          {post.map(user => {
            const { id, title } = user;
            return (
              <div
                key={id}
                className="post"
              >
                <span>{title}</span>
              </div>
            )
          })}
        </div>
      </div>
    );
  }
}

export default UserPost;

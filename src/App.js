import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Users from "./layouts/users/usersContainer";
import UserPost from "./layouts/post/userPostContainer";

const ROUTES = [
  {
    component: Users,
    path: "/",
    key: "users",
  },
  {
    component: UserPost,
    path: "/user/:userId",
    key: "posts",
  }
]

function App() {
  return (
    <BrowserRouter>
      <Switch>
        {ROUTES.map(route => {
          const { path, component, key } = route;
          return (
            <Route
              key={key}
              exact
              path={path}
              component={component}
            />
          )
        })}
      </Switch>
    </BrowserRouter>
  );
}

export default App;

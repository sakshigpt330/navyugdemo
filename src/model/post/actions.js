import axios from 'axios';
import { API_URL } from '../../config';
import {
  GET_POST_SUCCESS,
  GET_POST_FAILURE,
  GET_POST_STARTED,
} from './types';
  
export const fetchPost = (id, history) => {
  return dispatch => {
    dispatch(fetchPostStarted());
    axios
      .get(`${API_URL}posts?userId=${id}`)
      .then(res => {
        dispatch(fetchPostSuccess(res.data));
        history.push(`/user/:${id}`)
      })
      .catch(err => {
        dispatch(fetchPostFailure(err.message));
      });
  };
};

const fetchPostSuccess = post => ({
  type: GET_POST_SUCCESS,
  payload: post
});

const fetchPostStarted = () => ({
  type: GET_POST_STARTED
});

const fetchPostFailure = error => ({
  type: GET_POST_FAILURE,
  payload: {
    error
  }
});
import {
    GET_POST_SUCCESS,
    GET_POST_FAILURE,
    GET_POST_STARTED,
  } from './types';
  
  const initialState = {
    loading: false,
    post: [],
    error: null
  };
  
export default function postReducer(state = initialState, action) {
    switch (action.type) {
      case GET_POST_STARTED:
        return {
          ...state,
          post: [],
          loading: true
        };
      case GET_POST_SUCCESS:
        return {
          ...state,
          loading: false,
          error: null,
          post: action.payload
        };
      case GET_POST_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };
      default:
        return state;
    }
}